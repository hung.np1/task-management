import * as env from 'dotenv'
env.config({ path: '.env' })

import { Test, TestingModule } from '@nestjs/testing'
import { INestApplication } from '@nestjs/common'
import * as request from 'supertest'
import { TypeOrmModule } from '@nestjs/typeorm'
import { TaskEntity } from '../src/entities/task.entity'
import { UserEntity } from '../src/entities/user.entity'
import { TypeOrmConfig } from '../src/database/config'
import { getConnection } from 'typeorm'
import { initDatabase } from './testDb'
import { UserService } from '../src/modules/v1/user/user.service'
import { UserModule } from '../src/modules/v1/user/user.module'


describe('UserController (e2e)', () => {
  let app: INestApplication
  TypeOrmConfig.database = 'test'
  TypeOrmConfig.logging = false

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        UserModule,
        TypeOrmModule.forRoot(TypeOrmConfig),
        TypeOrmModule.forFeature([TaskEntity, UserEntity])
      ],
      providers: [UserService]
    }).compile()

    app = moduleFixture.createNestApplication()
    await app.init()
    await initDatabase()
  })

  afterAll(async () => {
    await app.close()
  })

  // Delete users
  it('Should report unauthorized deleting user 3 by user 2', () => {
    return request(app.getHttpServer())
      .delete('/v1/users/3')
      .set('uuid', '2')
      .expect(403)
  })
  it('Should delete user 3 by user 1', () => {
    // create 2 tasks for user  (id 1, 2)
    request(app.getHttpServer())
      .post('/v1/tasks')
      .set('uuid', '3')
      .send({
        title: 'Test task 1',
        content: 'Task 1 content',
      })
      .expect(201)
    request(app.getHttpServer())
      .post('/v1/tasks')
      .set('uuid', '3')
      .send({
        title: 'Test task 2',
        content: 'Task 2 content',
      })
      .expect(201)
    // create 1 task for user 1 (id 3)
    request(app.getHttpServer())
      .post('/v1/tasks')
      .set('uuid', '1')
      .send({
        title: 'Test task 3',
        content: 'Task 3 content',
      })
      .expect(201)
    // make user 3 follow task 1 and 3
    request(app.getHttpServer()).post('/v1/tasks/1/follow').set('uuid', '3').expect(201)
    request(app.getHttpServer()).post('/v1/tasks/3/follow').set('uuid', '3').expect(201)
    // delete user 3
    return request(app.getHttpServer())
      .delete('/v1/users/3')
      .set('uuid', '1')
      .expect(200)
      .expect(async res => {
        expect(res.body.message).toEqual('success')
        const user3 = await getConnection().getRepository(UserEntity).findOne(3, { withDeleted: true, relations: ['followingTasks', 'tasks'] })
        expect(user3.deletedAt).not.toBeNull()
        expect(user3.tasks.length).toEqual(0)
        expect(user3.followingTasks.length).toEqual(0)
      })
  })
})