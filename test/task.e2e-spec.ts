import * as env from 'dotenv'
env.config({ path: '.env' })

import { Test, TestingModule } from '@nestjs/testing'
import { INestApplication } from '@nestjs/common'
import * as request from 'supertest'
import { TypeOrmModule } from '@nestjs/typeorm'
import { TaskEntity } from '../src/entities/task.entity'
import { UserEntity } from '../src/entities/user.entity'
import { TaskModule } from '../src/modules/v1/task/task.module'
import { TaskService } from '../src/modules/v1/task/task.service'
import { TypeOrmConfig } from '../src/database/config'
import { getConnection } from 'typeorm'
import { initDatabase } from './testDb'
import { TaskStatus } from '../src/enum/taskStatus.enum'


describe('TaskController (e2e)', () => {
  let app: INestApplication
  TypeOrmConfig.database = 'test'
  TypeOrmConfig.logging = false

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        TaskModule,
        TypeOrmModule.forRoot(TypeOrmConfig),
        TypeOrmModule.forFeature([TaskEntity, UserEntity])
      ],
      providers: [TaskService]
    }).compile()

    app = moduleFixture.createNestApplication()
    await app.init()
    await initDatabase()
  })

  afterAll(async () => {
    await app.close()
  })

  // Test auth
  it('Should reject request without uuid', () => {
    return request(app.getHttpServer()).get('/v1/tasks').expect(403)
  })

  it('Should reject invalid user', () => {
    return request(app.getHttpServer()).get('/v1/tasks').set('uuid', '4').expect(403)
  })
  // Create tasks
  it('Should create valid task 1', () => {
    const newTask = {
      title: 'Test task 1',
      content: 'Task 1 content',
    }
    return request(app.getHttpServer())
      .post('/v1/tasks')
      .set('uuid', '1')
      .send(newTask)
      .expect(201)
      .expect(res => {
        expect(res.body.data).toEqual(
          expect.objectContaining({
            id: 1,
            title: 'Test task 1',
            content: 'Task 1 content',
            status: 0,
            creatorId: 1
          })
        )
      })
  })
  it('Should create valid task 2', () => {
    const newTask = {
      title: 'Test task 2',
      content: 'Task 2 content',
    }
    return request(app.getHttpServer())
      .post('/v1/tasks')
      .set('uuid', '1')
      .send(newTask)
      .expect(201)
      .expect(res => {
        expect(res.body.data).toEqual(
          expect.objectContaining({
            id: 2,
            title: 'Test task 2',
            content: 'Task 2 content',
            status: 0,
            creatorId: 1
          })
        )
      })
  })
  it('Should create valid task 3', () => {
    const newTask = {
      title: 'Test task 3',
      content: '',
    }
    return request(app.getHttpServer())
      .post('/v1/tasks')
      .set('uuid', '2')
      .send(newTask)
      .expect(201)
      .expect(res => {
        expect(res.body.data).toEqual(
          expect.objectContaining({
            id: 3,
            title: 'Test task 3',
            content: '',
            status: 0,
            creatorId: 2
          })
        )
      })
  })
  // Update tasks
  it('Should update task 1 title, content, mark complete', () => {
    const updatedTask = {
      title: 'Task 1 title updated',
      content: 'Task 1 content updated',
      status: TaskStatus.completed
    }
    return request(app.getHttpServer())
      .put('/v1/tasks/1')
      .set('uuid', '1')
      .send(updatedTask)
      .expect(200)
      .expect(res => {
        expect(res.body.data).toEqual(
          expect.objectContaining({
            id: 1,
            title: 'Task 1 title updated',
            content: 'Task 1 content updated',
            status: TaskStatus.completed,
            creatorId: 1
          })
        )
      })
  })
  it('Should mark task 1 as incomplete', () => {
    const updatedTask = {
      status: TaskStatus.incompleted
    }
    return request(app.getHttpServer())
      .put('/v1/tasks/1')
      .set('uuid', '1')
      .send(updatedTask)
      .expect(200)
      .expect(res => {
        expect(res.body.data).toEqual(
          expect.objectContaining({
            id: 1,
            title: 'Task 1 title updated',
            content: 'Task 1 content updated',
            status: TaskStatus.incompleted,
            creatorId: 1
          })
        )
      })
  })
  it('Should mark task 2 as complete', () => {
    const updatedTask = {
      status: TaskStatus.completed
    }
    return request(app.getHttpServer())
      .put('/v1/tasks/2')
      .set('uuid', '1')
      .send(updatedTask)
      .expect(200)
      .expect(res => {
        expect(res.body.data).toEqual(
          expect.objectContaining({
            id: 2,
            title: 'Test task 2',
            content: 'Task 2 content',
            status: TaskStatus.completed,
            creatorId: 1
          })
        )
      })
  })
  it('Should report invalid task 5', () => {
    const updatedTask = {
      status: TaskStatus.completed
    }
    return request(app.getHttpServer())
      .put('/v1/tasks/5')
      .set('uuid', '1')
      .send(updatedTask)
      .expect(422)
  })
  it('Should report unauthorized updating task 1', () => {
    const updatedTask = {
      status: TaskStatus.completed
    }
    return request(app.getHttpServer())
      .put('/v1/tasks/1')
      .set('uuid', '2')
      .send(updatedTask)
      .expect(401)
  })
  // List tasks
  it('Should list 2 tasks of user 1', () => {
    return request(app.getHttpServer()).get('/v1/tasks').set('uuid', '1').expect(200)
      .expect(res => {
        const data = res.body.data
        expect(data.length).toEqual(2)
        const task1 = data.find(d => d.id === 1)
        const task2 = data.find(d => d.id === 2)
        expect(task1).toEqual(
          expect.objectContaining({
            id: 1,
            title: 'Task 1 title updated',
            content: 'Task 1 content updated',
            status: TaskStatus.incompleted,
            creatorId: 1
          })
        )
        expect(task2).toEqual(
          expect.objectContaining({
            id: 2,
            title: 'Test task 2',
            content: 'Task 2 content',
            status: TaskStatus.completed,
            creatorId: 1
          })
        )
      })
  })
  it('Should list 1 incomplete task of user 1', () => {
    return request(app.getHttpServer()).get('/v1/tasks?status=0').set('uuid', '1').expect(200)
      .expect(res => {
        const data = res.body.data
        expect(data.length).toEqual(1)
        expect(data[0]).toEqual(
          expect.objectContaining({
            id: 1,
            title: 'Task 1 title updated',
            content: 'Task 1 content updated',
            status: TaskStatus.incompleted,
            creatorId: 1
          })
        )
      })
  })
  it('Should list 1 completed task of user 1', () => {
    return request(app.getHttpServer()).get('/v1/tasks?status=1').set('uuid', '1').expect(200)
      .expect(res => {
        const data = res.body.data
        expect(data.length).toEqual(1)
        expect(data[0]).toEqual(
          expect.objectContaining({
            id: 2,
            title: 'Test task 2',
            content: 'Task 2 content',
            status: TaskStatus.completed,
            creatorId: 1
          })
        )
      })
  })
  // Follow tasks
  it('Should make user 2 follow task 1', () => {
    return request(app.getHttpServer()).post('/v1/tasks/1/follow').set('uuid', '2').expect(201)
      .expect(res => {
        expect(res.body.data.followingTasks.length).toEqual(1)
        expect(res.body.data.followingTasks[0]).toEqual(
          expect.objectContaining({
            id: 1,
            title: 'Task 1 title updated',
            content: 'Task 1 content updated',
            status: TaskStatus.incompleted,
            creatorId: 1
          })
        )
      })
  })
  it('Should make user 2 follow task 2', () => {
    return request(app.getHttpServer()).post('/v1/tasks/2/follow').set('uuid', '2').expect(201)
      .expect(res => {
        const data = res.body.data.followingTasks
        expect(data.length).toEqual(2)
        const task1 = data.find(d => d.id === 1)
        const task2 = data.find(d => d.id === 2)
        expect(task1).toEqual(
          expect.objectContaining({
            id: 1,
            title: 'Task 1 title updated',
            content: 'Task 1 content updated',
            status: TaskStatus.incompleted,
            creatorId: 1
          })
        )
        expect(task2).toEqual(
          expect.objectContaining({
            id: 2,
            title: 'Test task 2',
            content: 'Task 2 content',
            status: TaskStatus.completed,
            creatorId: 1
          })
        )
      })
  })
  // List following tasks
  it('Should list following tasks of user 2', () => {
    return request(app.getHttpServer()).get('/v1/tasks/following').set('uuid', '2').expect(200)
      .expect(res => {
        const data = res.body.data
        expect(data.length).toEqual(2)
        const task1 = data.find(d => d.id === 1)
        const task2 = data.find(d => d.id === 2)
        expect(task1).toEqual(
          expect.objectContaining({
            id: 1,
            title: 'Task 1 title updated',
            content: 'Task 1 content updated',
            status: TaskStatus.incompleted,
            creatorId: 1
          })
        )
        expect(task2).toEqual(
          expect.objectContaining({
            id: 2,
            title: 'Test task 2',
            content: 'Task 2 content',
            status: TaskStatus.completed,
            creatorId: 1
          })
        )
      })
  })
  // Unfollow tasks
  it('Should make user 2 unfollow task 1', () => {
    return request(app.getHttpServer()).post('/v1/tasks/1/unfollow').set('uuid', '2').expect(201)
      .expect(res => {
        expect(res.body.data.followingTasks.length).toEqual(1)
        expect(res.body.data.followingTasks[0]).toEqual(
          expect.objectContaining({
            id: 2,
            title: 'Test task 2',
            content: 'Task 2 content',
            status: TaskStatus.completed,
            creatorId: 1
          })
        )
      })
  })
  // Delete tasks
  it('Should report unauthorized deleting task 1', () => {
    return request(app.getHttpServer())
      .delete('/v1/tasks/1')
      .set('uuid', '2')
      .expect(401)
  })
  it('Should report invalid task 5', () => {
    return request(app.getHttpServer())
      .delete('/v1/tasks/5')
      .set('uuid', '2')
      .expect(422)
  })
  it('Should delete task 2', () => {
    return request(app.getHttpServer())
      .delete('/v1/tasks/2')
      .set('uuid', '1')
      .expect(200)
  })
})