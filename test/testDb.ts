import { UserEntity } from "../src/entities/user.entity"
import { UserRole } from "../src/enum/userRole.enum"
import { getConnection } from "typeorm"

export const initDatabase = async () => {
  // recreate database
  await getConnection().dropDatabase()
  await getConnection().synchronize()
  // init users table
  const userRepo = getConnection().getRepository(UserEntity)
  await userRepo.save(
    [
      {
        id: 1,
        role: UserRole.admin
      },
      {
        id: 2,
        role: UserRole.user
      },
      {
        id: 3,
        role: UserRole.user
      }
    ]
  )
}