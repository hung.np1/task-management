import * as env from 'dotenv'

env.config({ path: '.env' })

import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { initDocumentation } from './documentation'
import { AppValidationPipe } from './common/validation.pipe'
import { TransformInterceptor } from './common/transform.interceptor'
import { TimeoutInterceptor } from './common/timeout.interceptor'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  app.enableCors()
  app.useGlobalPipes(
    new AppValidationPipe({
      validationError: {
        target: false,
        value: false,
      },
    }),
  )

  app.useGlobalInterceptors(new TransformInterceptor(), new TimeoutInterceptor())

  initDocumentation(app, {
    version: '1.0',
    title: 'Task Management Api',
    description: 'Documentation about Task Management API',
    endpoint: '/docs',
  })

  await app.listen(process.env.NEST_PORT || 7000)
}

bootstrap()
