export enum TaskStatus {
  incompleted = 0,
  completed = 1,
}
