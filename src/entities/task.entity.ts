import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn, DeleteDateColumn, ManyToOne, JoinColumn } from 'typeorm'
import { BaseEntity } from '../common/base.entity'
import { ApiProperty } from '@nestjs/swagger'
import { IsInt, IsNotEmpty, IsOptional, IsString, MaxLength } from 'class-validator'
import { UserEntity } from './user.entity'

@Entity('tasks')
export class TaskEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number

  @IsNotEmpty()
  @IsString()
  @MaxLength(255)
  @ApiProperty()
  @Column('varchar', { name: 'title', nullable: true, length: 255 })
  title: string | null

  @IsOptional()
  @IsString()
  @MaxLength(255)
  @ApiProperty()
  @Column('text', { name: 'content', nullable: true })
  content: string | null

  @ApiProperty()
  @IsNotEmpty()
  @IsInt()
  @Column('tinyint', { name: 'status', default: 0 })
  status: number

  @ApiProperty()
  @IsNotEmpty()
  @IsInt()
  @Column('int', { name: 'creator_id' })
  creatorId: number

  @CreateDateColumn({ name: 'created_at', type: 'timestamp', nullable: true })
  createdAt: Date

  @UpdateDateColumn({ name: 'updated_at', type: 'timestamp', nullable: true })
  updatedAt: Date

  @DeleteDateColumn({ name: 'deleted_at', type: 'timestamp', nullable: true })
  deletedAt: Date

  @ManyToOne(() => UserEntity)
  @JoinColumn({ name: 'creator_id' })
  creator: UserEntity
}
