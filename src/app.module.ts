import { Module } from '@nestjs/common'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { DatabaseModule } from './database/database.module'
import { TaskModule } from './modules/v1/task/task.module'
import { UserModule } from './modules/v1/user/user.module'

@Module({
  imports: [DatabaseModule, TaskModule, UserModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
