import { ValidationPipe, Injectable, ValidationError } from '@nestjs/common'
import { ValidationException } from './validation.exception'

@Injectable()
export class AppValidationPipe extends ValidationPipe {
  protected exceptionFactory = (errors: ValidationError[]) => {
    throw new ValidationException(errors)
  }
}
