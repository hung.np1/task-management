import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { ApiResponse } from './response'

export interface Response<T> {
  data: T
}

@Injectable()
export class TransformInterceptor<T> implements NestInterceptor<T, Response<T>> {
  intercept(context: ExecutionContext, next: CallHandler): Observable<Response<T>> {
    const status = context.switchToHttp().getResponse().statusCode
    return next.handle().pipe(
      map((data) => {
        if (data instanceof ApiResponse) {
          return data as Response<T>
        }
        let message = ''
        if (data && data.message != '') {
          message = data.message
          delete data.message
        }
        return {
          code: status,
          message,
          data,
        }
      }),
    )
  }
}
