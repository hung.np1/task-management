/* eslint-disable @typescript-eslint/camelcase */
import { Injectable } from '@nestjs/common'
import { Repository } from 'typeorm'

@Injectable()
export class BaseService<T> {
  constructor(protected repo: Repository<T>) {}
}
