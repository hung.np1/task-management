import { IsNotEmpty, IsString, MaxLength, IsOptional, IsNumber, Min, IsIn } from 'class-validator'
import { ApiProperty } from '@nestjs/swagger'

export class CreateTaskDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @MaxLength(255)
  title: string

  @ApiProperty()
  @IsOptional()
  @IsString()
  @MaxLength(255)
  content: string

  @ApiProperty()
  @IsOptional()
  @IsIn([0, 1])
  status: number
}

export class ReadTaskDTO {
  @ApiProperty()
  @IsOptional()
  status: number

  @ApiProperty()
  @IsOptional()
  page: number

  @ApiProperty()
  @IsOptional()
  perPage: number
}

export class UpdateTaskDTO {
  @ApiProperty()
  @IsOptional()
  @IsString()
  @MaxLength(255)
  title: string

  @ApiProperty()
  @IsOptional()
  @IsString()
  @MaxLength(255)
  content: string

  @ApiProperty()
  @IsOptional()
  @IsIn([0, 1])
  status: number
}
