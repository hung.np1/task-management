import { Module, HttpModule } from '@nestjs/common'
import { TaskController } from './task.controller'
import { TaskService } from './task.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { TaskEntity } from '../../../entities/task.entity'
import { UserEntity } from '../../../entities/user.entity'

@Module({
  imports: [TypeOrmModule.forFeature([TaskEntity, UserEntity]), HttpModule],
  providers: [TaskService],
  exports: [TaskService],
  controllers: [TaskController],
})
export class TaskModule {}
