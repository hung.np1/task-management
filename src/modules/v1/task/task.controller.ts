import { Body, Controller, Get, Param, Post, Put, Delete, Query, UseGuards } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'
import { TaskService } from './task.service'
import { ApiResponse } from '../../../common/response'
import { CreateTaskDTO, ReadTaskDTO, UpdateTaskDTO } from './dto/task.dto'
import { AuthGuard } from '../auth/auth.guard'

@Controller('v1/tasks')
@ApiTags('Task')
@UseGuards(AuthGuard)
export class TaskController {
  constructor(public service: TaskService) {}

  @Get('/')
  async index(@Query() queries: ReadTaskDTO) {
    return ApiResponse.create(await this.service.read(queries))
  }

  @Post('/')
  async create(@Body() params: CreateTaskDTO) {
    return ApiResponse.create(await this.service.create(params))
  }

  @Put('/:id')
  async update(@Param('id') id: number | string, @Body() params: UpdateTaskDTO) {
    return ApiResponse.create(await this.service.update(id, params))
  }

  @Delete('/:id')
  async delete(@Param('id') id: number | string) {
    return ApiResponse.create(await this.service.delete(id))
  }

  @Post('/:id/follow')
  async follow(@Param('id') id: number | string) {
    return ApiResponse.create(await this.service.follow(id))
  }

  @Post('/:id/unfollow')
  async unfollow(@Param('id') id: number | string) {
    return ApiResponse.create(await this.service.unfollow(id))
  }

  @Get('/following')
  async following() {
    return ApiResponse.create(await this.service.readFollowing())
  }
}
