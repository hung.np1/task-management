/* eslint-disable @typescript-eslint/camelcase */
import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Request } from 'express'
import { TaskEntity } from '../../../entities/task.entity'
import { BaseService } from '../../../common/base.service'
import { REQUEST } from '@nestjs/core'
import { CreateTaskDTO, ReadTaskDTO, UpdateTaskDTO } from './dto/task.dto'
import { DEFAULT_PAGE, DEFAULT_PER_PAGE, DEFAULT_TASK_STATUS } from '../../../common/appConst'
import { plainToClass, plainToClassFromExist } from 'class-transformer'
import { UserEntity } from '../../../entities/user.entity'
import { Repository } from 'typeorm'

@Injectable()
export class TaskService extends BaseService<TaskEntity> {
  private userRepository: Repository<UserEntity>

  constructor(@InjectRepository(TaskEntity) repo, @InjectRepository(UserEntity) userRepo, @Inject(REQUEST) protected readonly request: Request) {
    super(repo)
    this.userRepository = userRepo
  }

  async read(queries: ReadTaskDTO): Promise<TaskEntity[]> {
    const page = queries.page || DEFAULT_PAGE
    const perPage = queries.perPage || DEFAULT_PER_PAGE
    const status = queries.status || DEFAULT_TASK_STATUS
    const taskQuery = await this.repo
      .createQueryBuilder('task')
      .select(['task.id', 'task.title', 'task.content', 'task.status', 'task.creatorId'])
      .where('task.creator_id = :userId', { userId: this.request.user.id })
      .skip((page - 1) * perPage)
      .take(perPage)
      .orderBy('task.created_at', 'DESC')
    if (queries.status) {
      taskQuery.andWhere('task.status = :status', { status })
    }
    return taskQuery.getMany()
  }

  async create(params: CreateTaskDTO): Promise<TaskEntity> {
    const task = plainToClass(TaskEntity, params)
    task.status = params.status || DEFAULT_TASK_STATUS
    task.creatorId = this.request.user.id
    return await this.repo.save(task)
  }

  async update(id: number | string, params: UpdateTaskDTO): Promise<TaskEntity> {
    let task = await this.repo.findOne(id)
    this.checkPermission(task, this.request.user)
    task = plainToClassFromExist(task, params)
    return await this.repo.save(task)
  }

  async delete(id: number | string) {
    const task = await this.repo.findOne(id)
    this.checkPermission(task, this.request.user)
    this.repo.softDelete({ id: task.id })
  }

  async follow(id: number | string) {
    const task = await this.repo.findOne(id)
    this.checkPermission(task)
    const user: UserEntity = await this.userRepository.findOne(this.request.user.id, {
      relations: ['followingTasks'],
    })
    const existingTask = user.followingTasks.find((t: TaskEntity) => t.id === +id)
    if (!existingTask) {
      user.followingTasks = user.followingTasks.concat([task])
      return await this.userRepository.save(user)
    } else {
      return 'already following task'
    }
  }

  async unfollow(id: number | string) {
    const task = await this.repo.findOne(id)
    this.checkPermission(task)
    const user: UserEntity = await this.userRepository.findOne(this.request.user.id, {
      relations: ['followingTasks'],
    })
    const existingTaskIdx = user.followingTasks.findIndex((t: TaskEntity) => t.id === +id)
    if (existingTaskIdx >= 0) {
      user.followingTasks.splice(existingTaskIdx, 1)
      return await this.userRepository.save(user)
    } else {
      return 'not following task'
    }
  }

  async readFollowing() {
    const user: UserEntity = await this.userRepository.findOne(this.request.user.id, {
      relations: ['followingTasks'],
    })
    return user.followingTasks
  }

  private checkPermission(task: TaskEntity, user: UserEntity = null) {
    if (!task) {
      throw new HttpException('Invalid task', HttpStatus.UNPROCESSABLE_ENTITY)
    }
    if (user && task.creatorId !== user.id) {
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED)
    }
  }
}
