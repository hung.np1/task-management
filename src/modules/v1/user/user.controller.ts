import { Body, Controller, Get, Param, Post, Put, Delete, Query, UseGuards } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'
import { UserService } from './user.service'
import { ApiResponse } from '../../../common/response'
import { RolesGuard } from '../auth/roles.guard'
import { Roles } from '../auth/roles.decorator'
import { UserRole } from '../../../enum/userRole.enum'
import { AuthGuard } from '../auth/auth.guard'

@Controller('v1/users')
@ApiTags('User')
@UseGuards(AuthGuard)
export class UserController {
  constructor(public service: UserService) {}

  @Delete('/:id')
  @UseGuards(RolesGuard)
  @Roles(UserRole.admin)
  async delete(@Param('id') id: number | string) {
    return ApiResponse.create(await this.service.delete(id))
  }
}
