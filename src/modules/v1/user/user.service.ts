/* eslint-disable @typescript-eslint/camelcase */
import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Request } from 'express'
import { BaseService } from '../../../common/base.service'
import { REQUEST } from '@nestjs/core'
import { UserEntity } from '../../../entities/user.entity'
import { TaskEntity } from '../../../entities/task.entity'
import { getManager } from 'typeorm'

@Injectable()
export class UserService extends BaseService<UserEntity> {
  constructor(@InjectRepository(UserEntity) repo, @Inject(REQUEST) protected readonly request: Request) {
    super(repo)
  }

  async delete(id: number | string) {
    if (this.request.user.id === +id) {
      throw new HttpException('Cannot delete yourself', HttpStatus.UNPROCESSABLE_ENTITY)
    }
    const user = await this.repo.findOne(id, {
      relations: ['tasks'],
    })
    if (!user) {
      throw new HttpException('User not existing', HttpStatus.UNPROCESSABLE_ENTITY)
    }
    await getManager().transaction(async (entityManager) => {
      const userRepo = entityManager.getRepository(UserEntity)
      // unfollow tasks
      user.followingTasks = []
      await userRepo.save(user)
      // remove created tasks of user (soft delete)
      const taskIds = user.tasks.map((t) => t.id)
      if (taskIds.length > 0) {
        const taskRepo = entityManager.getRepository(TaskEntity)
        await taskRepo.softDelete(taskIds)
      }
      // finally soft delete user
      await userRepo.softDelete({ id: user.id })
    })
  }
}
