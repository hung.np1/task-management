import { CanActivate, ExecutionContext, HttpException, HttpStatus, Injectable } from '@nestjs/common'
import { UserEntity } from '../../../entities/user.entity'
import { getConnection } from 'typeorm'

@Injectable()
export class AuthGuard implements CanActivate {
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest()
    if (!request.headers.uuid) {
      throw new HttpException('no user uuid', HttpStatus.FORBIDDEN)
    }

    const user = await this.getUser(request.headers.uuid)

    if (!user) {
      throw new HttpException('invalid user uuid', HttpStatus.FORBIDDEN)
    }

    request.user = user

    return true
  }

  async getUser(id: number) {
    const userRepository = getConnection().getRepository(UserEntity)
    return await userRepository.findOne(id)
  }
}
