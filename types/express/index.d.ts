import { UserEntity } from '../../src/entities/user.entity'

import { Request } from 'express'

declare module 'express' {
  export interface Request {
    user: UserEntity
  }
}
