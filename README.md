# Quick start guide
1. Clone this project
```sh
git clone ...
```
2. Create a database
```sql
CREATE DATABASE `task_management` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
```
3. Install packages
```sh
yarn
```
4. Setup environment variables
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_USERNAME=root
DB_PASSWORD=root
DB_DATABASE=task_management
```
5. Start nestjs server
```sh
yarn start:dev
```
Here, typeorm synchronization will run to create tables based on implemented entities.

And the server will start at the default port 7000, unless we change the env `NEST_PORT=XXX`

6. Manually insert users to begin testing the apis
```sql
INSERT INTO `task_management`.`users` (`id`) VALUES (1), (2), (3);
```

# Testing
1. Create a database
```sql
CREATE DATABASE `test` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
```
2. Run test command
```sh
yarn test:e2e
```

# APIs
Swagger document at `/docs` (incompleted)
## Create tasks
- Endpoint: `POST /v1/tasks`
- Headers
```
content-type: application/json
uuid: USER_ID // simulate authenticated user
```
- Request body
```json
{
    "title": "Task title", // required
    "content": "Task content", // optional
    "status": 0 // optional, default is 0 (incompleted)
}
```
- Response (success)
```json
{
    "message": "success",
    "data": {
        ... // created task
    }
}
```

## Update a task
- Endpoint: `PUT /v1/tasks/:id`
- Headers
```
content-type: application/json
uuid: USER_ID // simulate authenticated user
```
- Request body
```json
{
    "title": "Task title", // optional
    "content": "Task content", // optional
    "status": 0 // optional, 0: incompleted, 1: completed
}
```
- Response (success)
```js
{
    message: 'success',
    data: {
        ... // updated task
    }
}
```

## Delete a task
- Endpoint: `DELETE /v1/tasks/:id`
- Headers
```
uuid: USER_ID // simulate authenticated user
```
- Request body: none
- Response (success)
```js
{
    message: 'success',
}
```

## Follow a task
- Endpoint: `POST /v1/tasks/:id/follow`
- Headers
```
uuid: USER_ID // simulate authenticated user
```
- Request body: none
- Response (success)
```js
{
    message: 'success',
    data: {
        id: 1 // current user id
        followingTasks: [
            {
                // following task content
            },
        ]
    }
}
```

## Unfollow a task
- Endpoint: `POST /v1/tasks/:id/unfollow`
- Headers
```
uuid: USER_ID // simulate authenticated user
```
- Request body: none
- Response (success)
```js
{
    message: 'success',
    data: {
        id: 1 // current user id
        followingTasks: [
            {
                // following task content
            },
        ]
    }
}
```

## Get following tasks
- Endpoint: `GET /v1/tasks/following`
- Headers
```
uuid: USER_ID // simulate authenticated user
```
- Request body: none
- Response (success)
```js
{
    message: 'success',
    data: [
        {
            // following task content
        },
    ]
}
```

## Delete a user
- Endpoint: `DELETE /v1/users/:id`
- Headers
```
uuid: USER_ID // simulate authenticated user
```
- Request body: none
- Response (success)
```js
{
    message: 'success',
}
```